package de.me

import org.testcontainers.containers.KafkaContainer
import org.testcontainers.dynamodb.DynaliteContainer
import org.testcontainers.utility.DockerImageName

abstract class Containers {
    companion object {
        private const val kafkaStandard = "confluentinc/cp-kafka:7.2.1"
        private const val kafkaArm64 = "hivecell/confluent-platform-arm64:kafka-5.5"
        private const val dynaliteStandard = "quay.io/testcontainers/dynalite:v1.2.1-1"

        // TODO look for replacement container
        // private const val dynaliteArm64 = "amazon/dynamodb-local:latest" // not compatible since it is not dynalite :D
        val kafkaContainer = createKafkaContainer()
        val dynaLiteContainer = DynaliteContainer(
            DockerImageName.parse(dynaliteStandard)
        )
        val setUpContainers = setUp()

        @JvmStatic
        fun setUp() {
            dynaLiteContainer.start()
            kafkaContainer.start()
        }

        private fun createKafkaContainer(): KafkaContainer {
            return if (isArm64()) {
                KafkaContainer(
                    DockerImageName.parse(kafkaArm64)
                        .asCompatibleSubstituteFor(kafkaStandard)
                )
            } else {
                KafkaContainer(
                    DockerImageName.parse(kafkaStandard)
                )
            }
        }

        private fun isArm64(): Boolean {
            val osType = System.getProperty("os.arch")
            return osType.equals("aarch64")
        }
    }
}
