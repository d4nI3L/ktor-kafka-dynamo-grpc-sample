package de.me.adapter.dynamo

import io.ktor.server.config.ApplicationConfig
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedAsyncClient
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient
import java.net.URI

object DynamoDbClients {

    fun getForEnvironmentConfig(dynamoDbConfig: ApplicationConfig, development: Boolean): DynamoDbEnhancedAsyncClient {
        val region = dynamoDbConfig.property("region").getString()
        val dynamoDbClient: DynamoDbAsyncClient = if (development) {
            val endpoint = dynamoDbConfig.property("endpoint").getString()
            val secretKey = dynamoDbConfig.property("secret-key").getString()
            val accessKeyId = dynamoDbConfig.property("access-key-id").getString()
            dynamoLocalClient(
                region = region,
                endpoint = endpoint,
                secretKey = secretKey,
                accessKeyId = accessKeyId
            )
        } else {
            dynamoClient(region = region)
        }
        return DynamoDbEnhancedAsyncClient.builder()
            .dynamoDbClient(dynamoDbClient)
            .build()
    }

    private fun dynamoLocalClient(
        region: String,
        secretKey: String,
        accessKeyId: String,
        endpoint: String
    ): DynamoDbAsyncClient =
        DynamoDbAsyncClient.builder()
            .region(Region.of(region))
            .endpointOverride(URI.create(endpoint))
            .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(accessKeyId, secretKey)))
            .build()

    private fun dynamoClient(region: String): DynamoDbAsyncClient =
        DynamoDbAsyncClient.builder()
            .region(Region.of(region))
            .build()
}
