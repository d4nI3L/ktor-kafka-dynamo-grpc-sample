package de.me.domain.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.Instant
import java.util.*

data class Event(
    @JsonProperty("userId")
    val userId: UUID,
    @JsonProperty("timestamp")
    val timestamp: Instant,
    @JsonProperty("message")
    val message: String
)
