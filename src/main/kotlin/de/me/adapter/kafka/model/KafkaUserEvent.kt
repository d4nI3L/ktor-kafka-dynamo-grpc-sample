package de.me.adapter.kafka.model

import com.fasterxml.jackson.annotation.JsonProperty
import de.me.domain.model.Event
import java.time.Instant
import java.util.UUID

data class KafkaUserEvent(
    @JsonProperty("eventId")
    val eventId: UUID,
    @JsonProperty("userId")
    val userId: UUID,
    @JsonProperty("timestamp")
    val timestamp: Instant,
    @JsonProperty("message")
    val message: String
)

fun KafkaUserEvent.toEvent(): Event =
    Event(
        userId = this.userId,
        timestamp = this.timestamp,
        message = this.message
    )

fun Event.toKafkaEvent(): KafkaUserEvent =
    KafkaUserEvent(
        eventId = UUID.randomUUID(),
        userId = this.userId,
        timestamp = this.timestamp,
        message = this.message
    )
