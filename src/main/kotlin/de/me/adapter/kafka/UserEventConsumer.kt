package de.me.adapter.kafka

import de.me.adapter.kafka.model.toEvent
import de.me.application.ClosableJob
import de.me.domain.service.EventRepository
import io.ktor.server.application.ApplicationEnvironment
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.slf4j.LoggerFactory
import java.util.*

class UserEventConsumer<String, KafkaUserEvent>(
    consumer: KafkaConsumer<String, KafkaUserEvent>,
    topic: kotlin.String,
    private val repository: EventRepository
) : Consumer<String, KafkaUserEvent>(consumer, topic), ClosableJob {

    override fun process(record: ConsumerRecord<String, KafkaUserEvent>) {
        val kEvent = record.value() as de.me.adapter.kafka.model.KafkaUserEvent
        logger.info("Saving event to dynamodb...")
        try {
            repository.save(event = kEvent.toEvent(), key = kEvent.eventId)
            logger.info("Successfully saved event")
        } catch (e: Exception) {
            logger.error("Encountered error saving event ${kEvent.eventId}")
            throw e
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(UserEventConsumer::class.java)
    }
}

fun<String, KafkaUserEvent> buildUserEventConsumer(environment: ApplicationEnvironment, eventRepository: EventRepository): UserEventConsumer<String, KafkaUserEvent> {
    val consumerConfig = environment.config.config("ktor.kafka.consumer")
    val consumerProps = Properties().apply {
        this[ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG] = consumerConfig.property("bootstrap-servers").getString()
        this[ConsumerConfig.CLIENT_ID_CONFIG] = consumerConfig.property("client-id").getString()
        this[ConsumerConfig.GROUP_ID_CONFIG] = consumerConfig.property("group-id").getString()
        this[ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG] = consumerConfig.property("key-deserializer").getString()
        this[ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG] = consumerConfig.property("value-deserializer").getString()
        this[ConsumerConfig.AUTO_OFFSET_RESET_CONFIG] = consumerConfig.property("offset").getString()
    }
    return UserEventConsumer(KafkaConsumer(consumerProps), consumerConfig.property("topic").getString(), eventRepository)
}
