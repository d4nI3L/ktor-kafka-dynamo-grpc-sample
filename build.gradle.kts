
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.owasp.dependencycheck.gradle.extension.AnalyzerExtension

val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val prometeus_version: String by project
val testcontainers_version: String by project
val aws_sdk_version: String by project

plugins {
    val kotlinVersion = "1.8.22"
    id("org.jetbrains.kotlin.jvm") version kotlinVersion
    id("org.jetbrains.kotlin.kapt") version kotlinVersion
    kotlin("plugin.serialization") version kotlinVersion
    id("io.ktor.plugin") version "2.3.1"
    id("org.owasp.dependencycheck") version "8.2.1"
    id("org.jlleitschuh.gradle.ktlint") version "11.4.0"
    id("org.jlleitschuh.gradle.ktlint-idea") version "11.4.0"

    idea
}

group = "de.me"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

application {
    mainClass.set("io.ktor.server.netty.EngineMain")

    val isDevelopment: Boolean = project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=$isDevelopment")
}

repositories {
    mavenCentral()
    gradlePluginPortal()
}

dependencies {

    implementation("io.ktor:ktor-server-cors-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-core-jvm:$ktor_version")

    implementation("io.ktor:ktor-server-metrics-micrometer-jvm:$ktor_version")
    // use specific micrometer dependency you require, e.g. DataDog, and configure it in the application code
    implementation("io.micrometer:micrometer-registry-prometheus:$prometeus_version")

    implementation("io.ktor:ktor-server-content-negotiation-jvm:$ktor_version")
    implementation("io.ktor:ktor-serialization-jackson-jvm:$ktor_version")
    implementation("io.ktor:ktor-serialization-jackson:$ktor_version")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.15.2")

    implementation("io.ktor:ktor-server-netty-jvm:$ktor_version")
    implementation("io.ktor:ktor-server-config-yaml:$ktor_version")

    implementation("org.apache.kafka:kafka-clients:3.4.1")

    implementation("ch.qos.logback:logback-classic:$logback_version")
    implementation("net.logstash.logback:logstash-logback-encoder:7.3")

    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:2.15.2")

    // sadly aws kotlin sdk is still in beta :/
    implementation(platform("software.amazon.awssdk:bom:$aws_sdk_version"))
    implementation("software.amazon.awssdk:dynamodb")
    implementation("software.amazon.awssdk:dynamodb-enhanced")

    testImplementation("io.ktor:ktor-server-tests-jvm:$ktor_version")
    testImplementation("io.ktor:ktor-client-content-negotiation-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-content-negotiation:$ktor_version")

    testImplementation("org.junit.jupiter:junit-jupiter:5.9.3")
    // personal taste: awesome mocking!
    testImplementation("io.mockk:mockk:1.13.5")

    testImplementation("org.testcontainers:kafka:$testcontainers_version")
    testImplementation("org.testcontainers:junit-jupiter:$testcontainers_version")
    testImplementation("org.testcontainers:dynalite:$testcontainers_version")
    testImplementation("com.amazonaws:aws-java-sdk-dynamodb:1.12.486")

    implementation("org.instancio:instancio:1.0.4")
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "17"
        }
    }

    kapt {
        includeCompileClasspath = false
    }

    withType<Test> {
        failFast = true
        useJUnitPlatform()
    }

    dependencyCheck {
        // disable Node.js and .NET scans
        analyzers(
            closureOf<AnalyzerExtension> {
                nodeEnabled = false
                nodeAuditEnabled = false
                assemblyEnabled = false
            }
        )
        val minimumRelevantCvvsScoreDefinedByIsec = 5f
        failBuildOnCVSS = minimumRelevantCvvsScoreDefinedByIsec
        scanConfigurations = listOf("runtimeClasspath")
        suppressionFile = "owasp-dependency-check/suppressions.xml"
    }
}
