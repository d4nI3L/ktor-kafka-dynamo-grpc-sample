package de.me

import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.parallel.Execution
import org.junit.jupiter.api.parallel.ExecutionMode

@Execution(ExecutionMode.SAME_THREAD)
internal annotation class Sequential

@Sequential
@Tag("integrationTest")
internal annotation class IntegrationTest
