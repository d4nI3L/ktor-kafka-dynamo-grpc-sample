package de.me.adapter.rest.model

import com.fasterxml.jackson.annotation.JsonProperty
import de.me.domain.model.Event
import java.time.Instant
import java.util.UUID

data class ExternalUserEvent(
    @JsonProperty("message")
    val message: String,
    @JsonProperty("timestamp")
    val timestamp: Instant
)

fun ExternalUserEvent.toDomainEvent(userId: UUID): Event =
    Event(
        userId = userId,
        message = this.message,
        timestamp = this.timestamp
    )
