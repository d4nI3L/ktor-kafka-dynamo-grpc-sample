package de.me.adapter.kafka.serialization

import de.me.adapter.kafka.model.KafkaUserEvent
import de.me.application.ApplicationObjectMapper
import org.apache.kafka.common.KafkaException
import org.apache.kafka.common.serialization.Deserializer
import org.slf4j.LoggerFactory

class UserEventDeserializer : Deserializer<KafkaUserEvent?> {

    override fun deserialize(topic: String?, data: ByteArray?): KafkaUserEvent? {
        try {
            if (data == null) {
                return null
            }
            return objectMapper.readValue(data, KafkaUserEvent::class.java)
        } catch (e: Exception) {
            throw DeserializationException("Error deserializing JSON message", e)
        }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(UserEventDeserializer::class.java)
        private val objectMapper = ApplicationObjectMapper.get()
    }
}

class DeserializationException(message: String, cause: Throwable) : KafkaException()
