package de.me.adapter.kafka

import de.me.application.ClosableJob
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.errors.WakeupException
import org.slf4j.LoggerFactory
import java.time.Duration
import java.time.temporal.ChronoUnit
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

abstract class Consumer<K, V>(private val consumer: KafkaConsumer<K, V>, private val topic: String) : ClosableJob {
    private val closed: AtomicBoolean = AtomicBoolean(false)
    private var finished = CountDownLatch(1)

    init {
        this.consumer.subscribe(listOf(this.topic))
    }

    override fun run() {
        try {
            while (!closed.get()) {
                val records = consumer.poll(Duration.of(100, ChronoUnit.MILLIS))
                for (record in records) {
                    logger.info("topic = ${record.topic()}, partition = ${record.partition()}, offset = ${record.offset()}, key = ${record.key()}, value = ${record.value()}")
                }
                if (!records.isEmpty) {
                    records.forEach { process(it) }
                    consumer.commitAsync { offsets, exception ->
                        if (exception != null) {
                            logger.error("Commit failed for offsets $offsets", exception)
                        } else {
                            logger.info("Offset committed  $offsets")
                        }
                    }
                }
            }
            logger.info("Finish consuming")
        } catch (e: Throwable) {
            when (e) {
                is WakeupException -> logger.info("Consumer waked up")
                else -> logger.error("Polling failed", e)
            }
        } finally {
            logger.info("Commit offset synchronously")
            consumer.commitSync()
            consumer.close()
            finished.countDown()
            logger.info("Consumer successfully closed")
        }
    }

    abstract fun process(record: ConsumerRecord<K, V>)

    override fun close() {
        logger.info("Close job...")
        closed.set(true)
        consumer.wakeup()
        finished.await(3000, TimeUnit.MILLISECONDS)
        logger.info("Job is successfully closed")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(Consumer::class.java)
    }
}
