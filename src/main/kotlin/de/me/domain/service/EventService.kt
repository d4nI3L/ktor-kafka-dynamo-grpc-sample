package de.me.domain.service

import de.me.domain.model.Event

interface EventService {
    fun process(event: Event)
}
