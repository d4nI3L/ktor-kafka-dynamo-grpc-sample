package de.me

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.dynamodb.DynamoDbClient
import software.amazon.awssdk.services.dynamodb.model.ProvisionedThroughput
import java.net.URI

object DynamoHelper {
    fun dynamoLocalEnhancedTestClient(
        region: String,
        secretKey: String,
        accessKeyId: String,
        endpoint: String
    ): DynamoDbEnhancedClient {
        val client = DynamoDbClient.builder()
            .region(Region.of(region))
            .endpointOverride(URI.create(endpoint))
            .credentialsProvider(StaticCredentialsProvider.create(AwsBasicCredentials.create(accessKeyId, secretKey)))
            .build()
        return DynamoDbEnhancedClient.builder()
            .dynamoDbClient(client)
            .build()
    }

    fun DynamoDbTable<*>.createTableForTest() = createTable { builder ->
        val provisionedThroughput = ProvisionedThroughput
            .builder()
            .readCapacityUnits(1L)
            .writeCapacityUnits(1L)
            .build()
        builder.provisionedThroughput(provisionedThroughput)
    }
}
