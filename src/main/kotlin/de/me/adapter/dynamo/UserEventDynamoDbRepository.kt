package de.me.adapter.dynamo

import de.me.adapter.dynamo.model.DynamoDbUserEvent
import de.me.adapter.dynamo.model.toDynamoDbEvent
import de.me.adapter.dynamo.model.toEvent
import de.me.domain.model.Event
import de.me.domain.service.EventRepository
import io.ktor.server.application.ApplicationEnvironment
import org.slf4j.LoggerFactory
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbAsyncTable
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedAsyncClient
import software.amazon.awssdk.enhanced.dynamodb.TableSchema
import software.amazon.awssdk.enhanced.dynamodb.model.PutItemEnhancedRequest
import software.amazon.awssdk.enhanced.dynamodb.model.ScanEnhancedRequest
import java.util.UUID
import java.util.function.Consumer

class UserEventDynamoDbRepository(
    private val client: DynamoDbEnhancedAsyncClient,
    private val dynamoDbTable: DynamoDbAsyncTable<DynamoDbUserEvent>
) : EventRepository {

    override fun save(event: Event, key: UUID) {
        logger.info("Saving event from ${event.timestamp} with message ${event.message}")
        val dynamoDbEvent = event.toDynamoDbEvent(key)
        val putItemRequest = PutItemEnhancedRequest.builder(DynamoDbUserEvent::class.java).item(dynamoDbEvent).build()
        dynamoDbTable.putItem(putItemRequest).join()
    }

    override fun getAllEntities(): List<Event> {
        val dynamoDbEvents = ArrayList<DynamoDbUserEvent>()
        val scanEnhancedRequest = ScanEnhancedRequest.builder()
            .limit(10).build()

        val iterator = dynamoDbTable.scan(scanEnhancedRequest)
        iterator.subscribe(
            Consumer { page ->
                page.items().forEach(dynamoDbEvents::add)
            }
        ).join()
        return dynamoDbEvents.map { it.toEvent() }
    }

    companion object {
        private val logger = LoggerFactory.getLogger(UserEventDynamoDbRepository::class.java)
    }
}

fun buildUserEventRepository(environment: ApplicationEnvironment): UserEventDynamoDbRepository {
    val dynamoConfig = environment.config.config("ktor.dynamodb.user-events")
    val tableName = dynamoConfig.property("table-name").getString()
    val client = DynamoDbClients.getForEnvironmentConfig(dynamoConfig, development = environment.developmentMode)
    val dynamoDbTable = client.table(tableName, TableSchema.fromBean(DynamoDbUserEvent::class.java))
    return UserEventDynamoDbRepository(client, dynamoDbTable)
}
