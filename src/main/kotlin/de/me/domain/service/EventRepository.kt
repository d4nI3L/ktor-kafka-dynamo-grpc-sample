package de.me.domain.service

import de.me.domain.model.Event
import java.util.UUID
interface EventRepository {

    fun save(event: Event, key: UUID)

    fun getAllEntities(): List<Event>
}
