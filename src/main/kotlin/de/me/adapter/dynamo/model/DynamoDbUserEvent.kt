package de.me.adapter.dynamo.model

import de.me.domain.model.Event
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbAttribute
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSortKey
import java.time.Instant
import java.util.UUID

@DynamoDbBean
data class DynamoDbUserEvent(
    @get:DynamoDbPartitionKey
    @get:DynamoDbAttribute("eventId")
    var eventId: String = "",

    @get:DynamoDbSortKey
    @get:DynamoDbAttribute("userId")
    var userId: String = "",

    @get:DynamoDbAttribute("epochMillis")
    var epochMillis: Long? = null,

    @get:DynamoDbAttribute("message")
    var message: String? = null
)

fun DynamoDbUserEvent.toEvent() =
    Event(
        userId = UUID.fromString(this.userId),
        message = this.message ?: "",
        timestamp = epochMillis?.let { Instant.ofEpochMilli(it) } ?: Instant.MIN
    )

fun Event.toDynamoDbEvent(key: UUID): DynamoDbUserEvent =
    DynamoDbUserEvent(
        eventId = key.toString(),
        userId = userId.toString(),
        epochMillis = this.timestamp.toEpochMilli(),
        message = this.message
    )
