package de.me.adapter.rest

import de.me.adapter.rest.model.ExternalUserEvent
import de.me.adapter.rest.model.toDomainEvent
import de.me.domain.service.EventPublisher
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.Application
import io.ktor.server.application.call
import io.ktor.server.request.receive
import io.ktor.server.response.respond
import io.ktor.server.routing.post
import io.ktor.server.routing.routing
import java.util.*

fun Application.configureUserEventRouter(eventPublisher: EventPublisher) {
    routing {
        post("/event") {
            val externalEvent = call.receive<ExternalUserEvent>()
            val userId = UUID.randomUUID() // in a business app it would be resolved through JWT
            eventPublisher.publish(externalEvent.toDomainEvent(userId))
            call.response.status(HttpStatusCode.Created)
            call.respond(externalEvent)
        }
    }
}
