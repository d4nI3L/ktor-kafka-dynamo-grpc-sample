package de.me

import de.me.DynamoHelper.createTableForTest
import de.me.DynamoHelper.dynamoLocalEnhancedTestClient
import de.me.adapter.dynamo.model.DynamoDbUserEvent
import io.ktor.server.config.MapApplicationConfig
import org.junit.jupiter.api.BeforeEach
import org.slf4j.LoggerFactory
import software.amazon.awssdk.enhanced.dynamodb.TableSchema
abstract class BaseIntegrationTest : Containers() {

    companion object {
        private val logger = LoggerFactory.getLogger(BaseIntegrationTest::class.java)
        private const val EVENTS_DYNAMO_TABLE = "events-table"
        private const val AWS_REGION = "eu-west-1"
        val kafkaTestConfiguration = MapApplicationConfig(
            "ktor.kafka.producer.client-id" to "test-producer",
            "ktor.kafka.producer.bootstrap-servers" to kafkaContainer.bootstrapServers,
            "ktor.kafka.producer.key-serializer" to "org.apache.kafka.common.serialization.StringSerializer",
            "ktor.kafka.producer.value-serializer" to "de.me.adapter.kafka.serialization.UserEventSerializer",
            "ktor.kafka.consumer.client-id" to "test-consumer",
            "ktor.kafka.consumer.group-id" to "test-group",
            "ktor.kafka.consumer.topic" to "events",
            // we have to set the offset to earliest in this case due to timing issues since ht topic gets created with
            // the first message and then the consumer joins with offset latest, hence 1 and not 0
            "ktor.kafka.consumer.offset" to "earliest",
            "ktor.kafka.consumer.bootstrap-servers" to kafkaContainer.bootstrapServers,
            "ktor.kafka.consumer.key-deserializer" to "org.apache.kafka.common.serialization.StringDeserializer",
            "ktor.kafka.consumer.value-deserializer" to "de.me.adapter.kafka.serialization.UserEventDeserializer",
            "ktor.dynamodb.user-events.region" to AWS_REGION,
            "ktor.dynamodb.user-events.table-name" to EVENTS_DYNAMO_TABLE,
            "ktor.dynamodb.user-events.secret-key" to dynaLiteContainer.credentials.credentials.awsSecretKey,
            "ktor.dynamodb.user-events.access-key-id" to dynaLiteContainer.credentials.credentials.awsAccessKeyId,
            "ktor.dynamodb.user-events.endpoint" to dynaLiteContainer.endpointConfiguration.serviceEndpoint
        )
    }

    @BeforeEach
    fun setupDynamoTables() {
        val enhancedClient = dynamoLocalEnhancedTestClient(
            region = AWS_REGION,
            secretKey = dynaLiteContainer.credentials.credentials.awsSecretKey,
            accessKeyId = dynaLiteContainer.credentials.credentials.awsAccessKeyId,
            endpoint = dynaLiteContainer.endpointConfiguration.serviceEndpoint
        )
        val table = enhancedClient.table(EVENTS_DYNAMO_TABLE, TableSchema.fromBean(DynamoDbUserEvent::class.java))
        try {
            table.deleteTable()
        } catch (e: Exception) {
            logger.info("Failed to delete dynamodb table $EVENTS_DYNAMO_TABLE. It may not exist jet.", e)
        }
        logger.info("Creating dynamodb table $EVENTS_DYNAMO_TABLE for tests...")
        table.createTableForTest()
    }
}
