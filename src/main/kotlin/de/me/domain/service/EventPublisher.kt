package de.me.domain.service

import de.me.domain.model.Event

interface EventPublisher {
    suspend fun publish(event: Event)
}
