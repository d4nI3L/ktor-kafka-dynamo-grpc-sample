package de.me.adapter.kafka

import de.me.adapter.kafka.model.KafkaUserEvent
import de.me.adapter.kafka.model.toKafkaEvent
import de.me.domain.model.Event
import de.me.domain.service.EventPublisher
import io.ktor.server.application.ApplicationEnvironment
import kotlinx.coroutines.suspendCancellableCoroutine
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.clients.producer.RecordMetadata
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class KafkaEventPublisher(private val producer: KafkaProducer<String, KafkaUserEvent>) : EventPublisher {
    override suspend fun publish(event: Event) {
        val kafkaEvent = event.toKafkaEvent()
        // some business logic...
        logger.info("Sending event ${kafkaEvent.eventId}")
        // using user Ids for partitioning (realistic for business apps)
        producer.dispatch(ProducerRecord(KAFKA_EVENT_TOPIC, kafkaEvent.userId.toString(), kafkaEvent))
    }

    companion object {
        private const val KAFKA_EVENT_TOPIC = "events"
        private val logger = LoggerFactory.getLogger(KafkaEventPublisher::class.java)
    }
}

fun buildKafkaUserEventPublisher(environment: ApplicationEnvironment): KafkaEventPublisher {
    val kafkaProducer = buildProducer<String, KafkaUserEvent>(environment)
    return KafkaEventPublisher(kafkaProducer)
}

fun <K, V> buildProducer(environment: ApplicationEnvironment): KafkaProducer<K, V> {
    val producerConfig = environment.config.config("ktor.kafka.producer")
    val producerProps = Properties().apply {
        this[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = producerConfig.property("bootstrap-servers").getString()
        this[ProducerConfig.CLIENT_ID_CONFIG] = producerConfig.property("client-id").getString()
        this[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = producerConfig.property("key-serializer").getString()
        this[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = producerConfig.property("value-serializer").getString()
        this[ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG] = "false"
    }
    return KafkaProducer(producerProps)
}

suspend inline fun <reified K : Any, reified V : Any> KafkaProducer<K, V>.dispatch(record: ProducerRecord<K, V>) =
    suspendCancellableCoroutine<RecordMetadata> { continuation ->
        this.send(record) { metadata, exception ->
            if (metadata == null) continuation.resumeWithException(exception!!) else continuation.resume(metadata)
        }.get()
    }
