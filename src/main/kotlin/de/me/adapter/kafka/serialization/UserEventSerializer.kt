package de.me.adapter.kafka.serialization

import com.fasterxml.jackson.core.JsonProcessingException
import de.me.application.ApplicationObjectMapper
import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.Serializer

class UserEventSerializer<T>() :
    Serializer<T> {
    override fun serialize(topic: String?, data: T?): ByteArray? {
        return if (data == null) {
            null
        } else {
            try {
                objectMapper.writeValueAsBytes(data)
            } catch (e: JsonProcessingException) {
                throw SerializationException("Error serializing JSON message", e)
            }
        }
    }

    companion object {
        private val objectMapper = ApplicationObjectMapper.get()
    }
}
