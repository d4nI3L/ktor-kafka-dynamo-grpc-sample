package de.me

import de.me.adapter.dynamo.buildUserEventRepository
import de.me.adapter.kafka.buildKafkaUserEventPublisher
import de.me.adapter.kafka.buildUserEventConsumer
import de.me.adapter.kafka.model.KafkaUserEvent
import de.me.adapter.rest.configureUserEventRouter
import de.me.application.BackgroundJob
import de.me.application.configureMonitoring
import de.me.application.configureSerialization
import io.ktor.server.application.Application
import io.ktor.server.application.install

fun main(args: Array<String>): Unit =
    io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // application.yaml references the main function. This annotation prevents the IDE from marking it as unused.
fun Application.module() {
    configureMonitoring()
    configureSerialization()
    val repository = buildUserEventRepository(environment)
    install(BackgroundJob.BackgroundJobPlugin) {
        name = "Kafka-Consumer-Job"
        job = buildUserEventConsumer<String, KafkaUserEvent>(environment, repository)
    }
    val eventPublisher = buildKafkaUserEventPublisher(environment)
    configureUserEventRouter(eventPublisher)
}
