# ktor-kafka-dynamo-grpc-sample (WIP)


This project explores how to use Ktor as an alternative to Spring Boot using Kafka, AWS DynamoDB, and gRPC
for modern event based cloud applications. Out of my own interest I want to investigate how those frameworks and technologies work together.
The implementation of a Kafka Listener/Producer, DynamoDB repository, and gRPC is more or less straight forward with Spring Boot.
However, sometimes Spring Boot is a bit of an overhead for very small domain specific application. Does all of this work with lightweight Ktor?
Could Ktor be an option alongside Spring Boot for small business applications?

This work is based and inspired on the following repositories/articles: 
* https://github.com/viartemev/ktor-kafka-example/ 
  * Based on ktor version 1
  * Code not usable with ktor version 2 without some adjust
  * Almost production ready
  * Very nice solution with the BackgroundJob implementation using Kotlin CoRoutines
  * Kudos to viartemev for the great work!
* https://gamov.io/workshop/2021/03/30/ktor-kafka-2021.html
  * Not as useful as example above
  * Nice integration of KStreams
  * Creating a topic within the application is a minus for me
* https://github.com/kiquetal/ktor-dynamodb-app
  * Basic of using async client and dynamodb

## Dev 
### Gradle - Build, Test, Lint

Build and run all tests

```shell
./gradlew clean build
```

Run tests
```shell
./gradlew test 
```

Add Ktlint pre-commit hook
```shell
./gradlew addKtlintCheckGitPreCommitHook
```

Run ktlint to format all files
```shell
./gradlew :ktlintFormat
```
