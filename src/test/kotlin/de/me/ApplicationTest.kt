package de.me

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import de.me.adapter.dynamo.buildUserEventRepository
import de.me.adapter.kafka.buildKafkaUserEventPublisher
import de.me.adapter.kafka.buildUserEventConsumer
import de.me.adapter.kafka.model.KafkaUserEvent
import de.me.adapter.rest.configureUserEventRouter
import de.me.adapter.rest.model.ExternalUserEvent
import de.me.application.BackgroundJob
import de.me.application.configureSerialization
import de.me.domain.service.EventRepository
import io.ktor.client.call.body
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.contentType
import io.ktor.serialization.jackson.jackson
import io.ktor.server.application.install
import io.ktor.server.testing.testApplication
import org.instancio.Instancio
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.lang.Thread.sleep
@IntegrationTest
class ApplicationTest : BaseIntegrationTest() {

    @Test
    fun `test controller and event processing`() = testApplication {
        var eRepository: EventRepository? = null
        val client = createClient {
            install(ContentNegotiation) {
                jackson {
                    registerModule(JavaTimeModule())
                }
            }
        }
        val randomEvent = Instancio.of(ExternalUserEvent::class.java).create()
        // val mapper = ApplicationObjectMapper.get()
        // val body = mapper.writeValueAsString(randomEvent)
        environment {
            config = kafkaTestConfiguration
        }
        application {
            configureSerialization()
            val repository = buildUserEventRepository(environment)
            eRepository = repository
            install(BackgroundJob.BackgroundJobPlugin) {
                name = "Kafka-Consumer-Job"
                job = buildUserEventConsumer<String, KafkaUserEvent>(environment, repository)
            }
            val eventPublisher = buildKafkaUserEventPublisher(environment)
            configureUserEventRouter(eventPublisher)
        }
        client.post(
            "/event"
        ) {
            setBody(randomEvent)
            contentType(ContentType.Application.Json)
        }.apply {
            assertEquals(HttpStatusCode.Created, status)
            assertEquals(randomEvent, body<ExternalUserEvent>())
        }
        assertDynamoDBTableHasItems(eRepository!!)
    }

    private fun assertDynamoDBTableHasItems(repository: EventRepository) {
        var b = true
        var c = 0
        while (b && c < 10) {
            try {
                val v = repository.getAllEntities()
                if (v.isNotEmpty()) {
                    b = false
                }
            } catch (e: Exception) {
                sleep(10000)
            }
            c++
        }
        assertTrue(repository.getAllEntities().isNotEmpty())
    }
}
